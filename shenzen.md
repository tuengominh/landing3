---
title: Research Trip
layout: page
---

{% for course in site.data.shenzen %}
 [{{course.name}}]({{course.url|relative_url}})
{% endfor %}
