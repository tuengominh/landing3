---
title: Living with Ideas
layout: "page"
order: 8
---
Temporal aspects of embodied living with prototypes

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/4b9ae9398764-fig20_modular_beercup_S.jpg)

### Faculty
Angella Mackey  
David McCallum  
Kristina Andersen  
Ron Wakkary

### Syllabus and Learning Objectives
Students will participate in a series of workshop activities that address challenges for quickly embodying concepts, and addressing them through lived experiences.

Throughout the week, students will engage in early and easy making processes. They will address experiences of these things through the body.
Each student will move through:
· Lo-fi version of their project/concept
· Different time scales
· Move from speculation to having a component of reality for their concept.

On the final day students will present an embodied concept.

### Total Duration
Classes: 3-6pm, Mon-Fri
Student work hours: 20 hours

### Structure and Phases
*Day 1*: Workshop: Make a magic machine. Facilitated by David McCallum and Kristina Andersen  
*Day 2*: Workshop: Wearing the digital. Facilitated by Angella Mackey  
*Day 3*: Workshop: Living with the thing. Presentation by Ron Wakkary. Facilitated by Angella Mackey  
*Day 4*: Work in the studio to build your idea.  
*Day 5*: Student presentations. Critique and discussion. Facilitated by David McCallum and Kristina Andersen  

### Output
Research artefacts, lo-fi version of project/concept.

### Grading Method
Participation and reflection required. 

### ANGELLA MACKEY
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/480a2f552e4c-AngellaMackey_2017_sm2.jpg)

### Email Address
<a.m.mackey@tue.nl>

### Professional BIO
The four instructors facilitating this workshop series span a wide range of skills and experience in interaction design, industrial design, wearables, fashion, media art, and design research.
