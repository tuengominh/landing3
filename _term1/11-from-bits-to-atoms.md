---
title: From Bits to Atoms
layout: "page"
order: 11
---

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/aea617f146c7-Armengol.jpg)

### Faculty
Santi Fuentemilla  
Xavi Dominguez  
Eduardo Chamorro  
Esteban Gimenez  

### Syllabus and Learning Objectives
Fab Labs and advanced manufacturing infrastructure are making accessible for any citizen to make anything anywhere while sharing it with global networks of knowledge, which allows accelerating design, development and deployment processes for new products to be born. Traditional planning and urbanism are being disrupted by the acceleration of technology and the dynamic transformation of society during the last half century; it is important to rethink how we make things and why, and generate active and practical conversations through projects and prototypes that become manifests itself.

Bits<>Atoms is a practical and intensive one-week training program and a broad-reaching introduction to the Fab Lab environment. It has been designed to fill knowledge gaps and aimed to prepare students to succeed and improve their experience during [Fab Academy](http://fabacademy.org/).

During this workshop, we will be going over the basic skills needed to design, develop and fabricate almost anything in a Fab Lab, as well as how to manage the resources necessary to its proper operation.

Our active learning methodology is based on the practice of small exercises, designed to encourage the creativity and imagination of the participants, as well as stimulate the search for tools and solutions for their correct definition.

The week provides the tools and skills for launching your ideas into the future of digital fabrication and distributed manufacturing. We will offer an impact experience, seeking to inspire and motivate the participants to use the possibilities of digital manufacturing and technologies to prototype, design, fabricate and program an "honest" mechanical artefact that "makes" something.  

### Total Duration
Classes: 10-13 p.m, Mon-Fri (15 per week)  
Student work hours: 15-19 p.m, Mon-Fri (20 per week)  
Total hours per student: 35 hours

### Structure and Phases
**D1 - Fablab, Fabacademy Introduction**
- Intro Fablab and Fabacademy
- Safe Rules and Hand tool training
- Documentation
- Machine design challenge (inspiration ideas, rules, documentation....)


**D2 - Design and Fabrication**
- Review presentations
- Digital fabrication intro (Laser cut, CNC, 3D printing)
- Parametric modeling

**D3 - Electronics**
- Review presentations
- Inputs: sensor fabrication
- Outputs: Motors, Stepper library, arduino 
- Gode, GRBL

**D4 - Interaction**
- Review presentations
- Based on the evolution and needs of the week, we will focus on Processing, Firefly, Python, appinventor, VR

**D5 - Presentation**
- Work hours  
- Final Presentations    

### Output
"honest" mechanical artifact (1 input + 2 output + replicable + 2 differents fabrication process)  

### Grading Method
- Documentacion, 50%  
- Participation: 30%  
- Presentation: 15%  
- If it does not explode: 5%  

### Background Research Material
[Fab Academy](http://fabacademy.org/)  
[Fab Foundation](http://www.fabfoundation.org/)   
[Fab Academy BCN](http://fab.academany.org/2018/labs/barcelona/local/general/)   
[Fab Zero](https://gitlab.fabcloud.org/fabzero/fabzero)   
[Daniel Armengol Altayo](http://archive.monograph.io/james/how-to-make-almost-anythinghttp://fabacademy.org/archives/nodes/barcelona/armengol_altayo.daniel/htm/18.html)   
[Machines that Make](http://mtm.cba.mit.edu/)   
[Designing Reality](http://designingreality.org/)    

[Make: The Maker's Manual: A Practical Guide to the New Industrial Revolution](https://www.amazon.es/Make-Makers-Practical-Industrial-Revolution/dp/145718592X/ref=tmm_pap_swatch_0?_encoding=UTF8&qid=1537539307&sr=1-1)   
[The 3D Printing Handbook: Technologies, design and applications](https://www.amazon.es/gp/product/9082748509/ref=as_li_qf_sp_asin_il_tl?ie=UTF8&tag=3dhu-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=9082748509&linkId=33a88790f9237a3e0c6eae63445b1948)   
[Fab: The Coming Revolution on Your Desktop - from Personal Computers to Personal Fabrication](https://www.amazon.es/Fab-Revolution-Personal-Computers-Fabrication/dp/0465027458/ref=pd_sim_14_4?_encoding=UTF8&pd_rd_i=0465027458&pd_rd_r=cc7aded7-f904-11e8-a234-ef904517ef02&pd_rd_w=qEj87&pd_rd_wg=GloRf&pf_rd_p=cc1fdbc2-a24a-4df6-8bce-e68491d548ae&pf_rd_r=8CBTMRHPVFRSQ0JWN5EP&psc=1&refRID=8CBTMRHPVFRSQ0JWN5EP)   

### Requirements for the Students
Motivation  

### Infrastructure Needs
Fablab facilities and Academy room

### Student' Artifacts
[From Bits to Atoms](https://bitsandatoms.gitlab.io/site/) 

### SANTI FUENTIMILLA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/3857bdcbd1cd-santi.jpg)

### Email Address
<santi@fablabbcn.org>  

### Twitter Account
@santifu  

### Professional BIO
Santiago Fuentemilla Garriga​ (male), with Master of Architecture from the University of la Salle Universitat Ramon LLull , Spain, as a specialist in Architectural Design and Construction. In 2012 he graduated from the Fab Academy Diploma at FabLab BCN, a digital fabrication and rapid prototyping course directed by Neil Gershenfeld at MIT ́s Center For Bits and Atoms (CBA). Currently, he is undertaking a PHD in digital fabrication processes at the EGA UPC (Universitat Politecnica de Catalunya). As a professional Santi has worked in various architectural firms carrying out projects at the international level in the last 10 years. He is currently the design director at OPR (Other people’s Rooms) in Barcelona, a multidisciplinary studio based on architectural concept design for enhanced user experiences. 
Since 2013 he is part of the [Fab Lab BCN](https://fablabbcn.org) team, he is the coordinator of the Future Learning Unit (FLU), the unit focused on the design, implementation and coordination of active learning experiences with digital manufacturing tools for the community. [FLU](https://twitter.com/futurelearningu) designs and promotes educational, innovation and entrepreneurship projects such as [AmbMakers](https://twitter.com/hashtag/ambmakers) [POPUPLAB "Digital Fabrication Everywhere"](https://fablabbcn.org/popup_fab_lab.html), [FABKIDS](http://kids.fablabbcn.org/), [CROCOPOI](https://crocopoi.com). FLU participates in European research projects such as [DOIT](https://www.doit-europe.net/) or [DSISCALE](https://digitalsocial.eu/about-the-project) and [PHALABS 4.0](http://www.phablabs.eu).
Since 2014 he is Fab Instructor of the global academic program [Fab Academy](http://fabacademy.org/) and since 2017 he is professor of the Master in Design for Emergent Futures [MDEF](https://iaac.net/educational-programmes/master-design-emergent-futures) organized by IAAC.