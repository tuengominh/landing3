---
title: Students' Projects
---

<!-- DO NOT CHANGE THIS! Instead edit the file /_data/student-projects -->
{% for project in site.data.student-projects %}
    <div class="">
      <div class="student-project">
        <h6>{{ project.name }} </h6>
        <a href="{{ project.url }}" class="image fit">
        <img src="{{ project.image | relative_url }}" />
        </a>
      </div>
    </div>
{% endfor %}
